package config;

import action.IndexController;
import com.jfinal.config.Routes;

public class SysRoutes extends Routes {
	public void config() {
		add("/", IndexController.class);
	}
}