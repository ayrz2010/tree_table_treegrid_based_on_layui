package config;

import com.jfinal.config.*;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.template.Engine;

public class SysConfig extends JFinalConfig {
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用PropKit.get(...)获取值
		PropKit.use("dl_config.txt");
		boolean devMode=PropKit.getBoolean("devMode", true);
		me.setDevMode(devMode);
	}
	
	/**
	 * 配置路由[action路径]
	 */
	public void configRoute(Routes me) {
		me.add(new SysRoutes());
	}

	public static DruidPlugin createDruidPlugin(String pFileName) {
		if(pFileName==null||pFileName.trim().equals("")){
			return null;
		}
		String url= PropKit.use(pFileName).get("jdbc.url");
		String username=PropKit.use(pFileName).get("jdbc.username");
		String password=PropKit.use(pFileName).get("jdbc.password");
		DruidPlugin dblugin = new DruidPlugin(url, username, password);
		return dblugin;
	}

	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 数据连接池
		DruidPlugin dblugin = createDruidPlugin("jdbc.properties");
		me.add(dblugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dblugin);
		arp.setShowSql(true);
		me.add(arp);
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		DruidStatViewHandler dvh =  new DruidStatViewHandler("/druid");
		me.add(dvh);
		me.add(new ContextPathHandler("path"));//假如这个
	}
	
	
	/**
	 * 系统启用前调用
	 */
	@SuppressWarnings("static-access")
	@Override
	public void afterJFinalStart() {
	}
	
	
	/**
	 * 系统关闭前调用
	 */
	@Override
	public void beforeJFinalStop() {
		super.beforeJFinalStop();
		//暂不处理
		System.out.println("系统正在关闭...");
	}

	public static Engine engine=null;
	@Override
	public void configEngine(Engine me) {
	}
}
